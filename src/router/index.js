import { createRouter, createWebHistory } from 'vue-router';

import Cart from '@/views/Cart';
import Checkout from '@/views/Checkout';
import Product from '@/views/Product';
import ProductList from '@/views/ProductList';
import E404 from '@/views/E404';

let routes = [
    {
        name: 'cart',
        path: '/cart',
        component: Cart
    },
    {
        name: 'checkout',
        path: '/checkout',
        component: Checkout
    },
    {
        name: 'catalog',
        path: '/',
        component: ProductList
    },
    {
        name: 'products-item',
        path: '/products/:id',
        component: Product
    },
    {
        name: 'E404',
        path: '/:pathMatch(.*)',
        component: E404
    }
]
export default createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
})